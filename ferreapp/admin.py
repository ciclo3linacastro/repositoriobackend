from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models.usuario import Usuario
from .models.producto import Producto
from .models.proveedores import Proveedores

admin.site.register(Usuario)
admin.site.register(Producto)
admin.site.register(Proveedores)