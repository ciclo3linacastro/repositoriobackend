from ferreapp.models.account import Productos
from rest_framework import serializers

class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'descripcion', 'preciounitario', 'iva', 'cantdisponible']