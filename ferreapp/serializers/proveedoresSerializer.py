from ferreapp.models.account import Proveedores
from rest_framework import serializers
from ferreapp.models.producto import Producto 
from ferreapp.serializers.productoSerializer import ProductoSerializer

class ProveedoresSerializer(serializers.ModelSerializer):
    producto = ProductoSerializer()
    class Meta:
        model = Proveedores 
        fields = ['idproveedor', 'nombre', 'correo', 'telefono', 'direccion']

    def to_representation(self, obj):
        proveedores = Proveedores.objects.get(idproveedor=obj.idproveedor)
        producto = Producto.objects.get(idproveedor=obj.idproveedor)
        return {
                    'idproveedor': proveedores.idproveedor,
                    'nombre': proveedores.nombre,
                    'correo': proveedores.correo,
                    'telefono': proveedores.telefono,
                    'producto': {
                        'id': producto.id,
                        'nombre': producto.nombre,
                        'descripcion': producto.descripcion,
                        'preciounitario': producto.preciounitario,
                        'iva': producto.iva,
                        'cantdisponible': producto.cantdisponible
                    }
                }