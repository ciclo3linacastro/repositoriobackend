from django.db import models
from .proveedores import Proveedores

class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length = 30)
    descripcion = models.CharField('Nombre', max_length = 90)
    preciounitario = models.IntegerField(default=0)
    iva = models.FloatField(default=0)
    cantdisponible = models.IntegerField(default=0)
    idproveedor = models.ForeignKey(Proveedores, related_name='producto', on_delete=models.CASCADE)