from django.db import models

class Proveedores(models.Model):
    idproveedor = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length = 30)
    correo = models.EmailField('Correo', max_length = 100)    
    telefono = models.IntegerField(default=0)
    direccion = models.CharField('direccion', max_length = 30)